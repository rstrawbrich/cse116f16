package PlayersOfGame;

import java.util.ArrayList;
import javax.swing.ImageIcon;
import Characters.*;
import Rooms.*;
import Weapons.*;
import edu.buffalo.cse116.AllCards;
import edu.buffalo.cse116.DiceRoll;

public class AllPlayers {

	private String _currentposition;
	private int _index, numberoftimesfordiceroll;
	private ArrayList<AllCards> _hand;
	private String _previousmove;
	private ImageIcon image;
	private int availablespacestotake,row,column;
	private AllCharacters _character;
	private boolean stateofplayer;
	//this class is setup as an exchange point of information for other classes to access certain values that will help the players traverse the game board correctly
	public AllPlayers(ArrayList<AllCards> hand,int index){
		_hand = hand;
		_index = index;
		stateofplayer = true;
		availablespacestotake = 0;
		numberoftimesfordiceroll = 0;
		_previousmove = "";
		image = null;
		_currentposition = "Hallway";
		setInitialConditions();
	}
	public void setPlayerState(boolean state){//method to allow a state of the players(true or false status) to be assigned to them to help with deciding whether they are remaining in the game after their final accusation
		stateofplayer = state;
	}
	public boolean getPlayerState(){
		return stateofplayer;
	}
	
	public String getcharacterName(){
		return _character.getName();
	}

	public int getIndex() {
		return _index;
	}

	public int getDiceRollTimes() {//stores how many times the dice was rolled by the current player
		return numberoftimesfordiceroll;
	}
	
	public String currentPosition(){//gives current position of the player
		return _currentposition;	
	}
	
	public void setCurrentPosition(String s){//set the current position of the player under circumstance
		_currentposition = s;
	}
	
	public String getPreviousMove(){
		return _previousmove;
	}
	
	public void setSteps(int i){
		availablespacestotake = i;
	}
	public boolean diceRoll() {//gives the number of steps that the player can use as a result of a "dice roll"
		if(numberoftimesfordiceroll == 0){
			DiceRoll dice = new DiceRoll();
			availablespacestotake = dice.Dice();
			numberoftimesfordiceroll += 1;
			return true;
		} else {
			return false;
		}
	}
	public int getAvailableSpacesToTake(){//how many spaces that the player currently has left to use
		return availablespacestotake;
	}
	public void availableSpaceIncrement(){
		availablespacestotake -= 1;
	}
	public ArrayList<AllCards> getCards(){
		return _hand;
	}
	public int currentRow(){
		return row;
	}
	public int currentColumn(){
		return column;
	}
	public void setRow(int i){
		row = i;
	}
	public void setColumn(int i){
		column = i;
	}
	public void setInitialConditions(){//illustrates where the players will start at the beginning of a new game, their characters, and their images
		if(_index==1){
			_character = new ProfPlum();
			row = 5;
			column = 0;
			image = new ImageIcon(getClass().getResource("ProfPlum.PNG"));
		}else if(_index==0){
			_character =  new MsScarlet();
			row = 0;
			column = 14;
			image = new ImageIcon(getClass().getResource("MsScarlet.PNG"));
		}else if(_index==5){
			_character = new ColMustard();
			row = 6;
			column = 21;
			image = new ImageIcon(getClass().getResource("ColMustard.PNG"));
		}else if(_index==2){
			_character =  new MrGreen();
			row = 21;
			column = 9;
			image = new ImageIcon(getClass().getResource("MrGreen.PNG"));
		}else if(_index==3){
			_character = new MrsWhite();
			row = 21;
			column = 10;
			image = new ImageIcon(getClass().getResource("MrsWhite.PNG"));
		}else if(_index==4){
			_character =  new MsPeacock();
			row = 17;
			column = 0;
			image = new ImageIcon(getClass().getResource("MsPeacock.PNG"));	
		}
	}
	
	public void exitRoomOption(){//tells the GUI where to place the player when the exit room button is pressed
		if(availablespacestotake!=0){
			
			if(_currentposition.equals("Study")){
				setCurrentPosition("doorspace");
				setRow(4);
				setColumn(5);
			}
			if (_currentposition.equals("Hall")) {
				setCurrentPosition("doorspace");
				setRow(6);
				setColumn(10);
			}
			if (_currentposition.equals("Lounge")) {
				setCurrentPosition("doorspace");
				setRow(5);
				setColumn(17);
			}
			if (_currentposition.equals("Library")) {
				setCurrentPosition("doorspace");
				setRow(8);
				setColumn(5);
			}
			if (_currentposition.equals("DiningRoom")) {
				setCurrentPosition("doorspace");
				setRow(7);
				setColumn(16);
			}
			if (_currentposition.equals("BillardRoom")) {
				setCurrentPosition("doorspace");
				setRow(13);
				setColumn(5);
			}
			if (_currentposition.equals("Conservatory")) {
				setCurrentPosition("doorspace");
				setRow(19);
				setColumn(4);
			}
			if (_currentposition.equals("BallRoom")) {
				setCurrentPosition("doorspace");
				setRow(13);
				setColumn(10);
			}
			if (_currentposition.equals("Kitchen")) {
				setCurrentPosition("doorspace");
				setRow(17);
				setColumn(15);
			}
		}

	}

	public boolean suggestion(AllCharacters character, AllWeapons weapon, AllRooms room) {
		// This method will see whether the each other the cards in the next
		// players hand matches the cards that have
		// been asked by the current player making the suggestion. If any of
		// them match then it will return true, if not then it will return
		// false.
		for (AllCards c : getCards()) {
			if (character.match(c)) {
				return true;
			}
			if (weapon.match(c)) {
				return true;
			}
			if (room.match(c)) {
				return true;
			}
		}
		return false;
	}

	public void moveUp() {// this illustrates the path of the player that cannot
							// be taken over again and updates the position of
							// the player
		row -= 1;
		_previousmove = "up";
		availablespacestotake -= 1;
	}

	public void moveDown() {// this illustrates the path of the player that
							// cannot be taken over again and updates the
							// position of the player
		row += 1;
		_previousmove = "down";
		availablespacestotake -= 1;
	}

	public void moveLeft() {// this illustrates the path of the player that
							// cannot be taken over again and updates the
							// position of the player
		column -= 1;
		_previousmove = "left";
		availablespacestotake -= 1;
	}

	public void moveRight() {// this illustrates the path of the player that
								// cannot be taken over again and updates the
								// position of the player
		column += 1;
		_previousmove = "right";
		availablespacestotake -= 1;
	}

	public int getNumberOfTimesFOrDiceRoll() {
		return numberoftimesfordiceroll;
	}

	public void resetTheNumberofTimesForDiceRoll() {//resets the dice roll attempts to zero
		numberoftimesfordiceroll = 0;
	}

	public void resetTheSteps() {//resets the current players allowed spaces to take back to zero
		availablespacestotake = 0;
	}

	public void resetPreviousMove() {//resets the players previous move so that they can back track the same path after the turn has gone through rotation
		_previousmove = "";
	}
	public void setTheNumberofTimesForDiceRoll() {//sets number of times the player can roll the dice to one
		numberoftimesfordiceroll = 1;
		
	}

	public ImageIcon getImageIcon(){//allows for retrieval of icons to display in the game board GUI
		return image;
	}
}

