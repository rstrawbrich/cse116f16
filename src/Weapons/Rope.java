package Weapons;

import javax.swing.ImageIcon;

import Rooms.Conservatory;

public class Rope extends AllWeapons{

	public Rope() {
		super();
		setInitialSpot(new Conservatory());
	}

	@Override
	public String getName() {
		return "Rope";
	}

	@Override
	public String getImage() {
		return "Rope.PNG";
	}
	@Override
	public ImageIcon getImageIcon(){
		return new ImageIcon(getClass().getResource("Rope.PNG"));
	}
	
}
