package Weapons;

import javax.swing.ImageIcon;

import Rooms.Study;

public class Revolver extends AllWeapons {

	public Revolver() {
		super();
		setInitialSpot(new Study());
	}

	@Override
	public String getName() {
		return "Revolver";
	}

	@Override
	public String getImage() {
		return "Revolver.PNG";
	}
	@Override
	public ImageIcon getImageIcon(){
		return new ImageIcon(getClass().getResource("Revolver.PNG"));
	}
}
