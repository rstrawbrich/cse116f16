package Weapons;



import javax.swing.ImageIcon;

import Rooms.Ballroom;


public class CandleStick extends AllWeapons {

	public CandleStick() {
		super();
		setInitialSpot(new Ballroom());	
	}

	@Override
	public String getName() {
		return "CandleStick";
	}

	@Override
	public String getImage() {
		return "CandleStick.PNG";
	}
	
	@Override
	public ImageIcon getImageIcon(){
		return new ImageIcon(getClass().getResource("Candlestick.PNG"));
	}
}
