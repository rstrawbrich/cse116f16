package Weapons;



import javax.swing.ImageIcon;

import Rooms.Hall;

public class Knife extends AllWeapons {

	public Knife() {
		super();
		setInitialSpot(new Hall());
	}

	@Override
	public String getName() {
		return "Knife";
	}

	@Override
	public String getImage() {
		return "Knife.PNG";
	}
	@Override
	public ImageIcon getImageIcon(){
		return new ImageIcon(getClass().getResource("knife.PNG"));

	}

}
