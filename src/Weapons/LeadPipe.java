package Weapons;

import javax.swing.ImageIcon;

import Rooms.Kitchen;

public class LeadPipe extends AllWeapons{


	public LeadPipe() {
		super();
		setInitialSpot(new Kitchen());
	}

	@Override
	public String getName() {
		return "LeadPipe";
	}

	@Override
	public String getImage() {
		return "LeadPipe.PNG";
	}
	@Override
	public ImageIcon getImageIcon(){
		return new ImageIcon(getClass().getResource("LeadPipe.PNG"));
	}
}
