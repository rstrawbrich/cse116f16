package Weapons;

import javax.swing.ImageIcon;

import Rooms.Lounge;

public class Wrench extends AllWeapons {

	public Wrench() {
		super();
		setInitialSpot(new Lounge());
	}

	@Override
	public String getName() {
		return "Wrench";
	}

	@Override
	public String getImage() {
		return "Wrench.PNG";
	}
	@Override
	public ImageIcon getImageIcon(){
		return new ImageIcon(getClass().getResource("Wrench.PNG"));
	}
}
