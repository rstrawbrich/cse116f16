package Weapons;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.ImageIcon;

import Rooms.AllRooms;
import edu.buffalo.cse116.AllCards;
import edu.buffalo.cse116.GameBoard;

public class AllWeapons extends AllCards {
	private String curpos;
	private int row, column;

	public AllWeapons() {
		super();		
		row= 0;
		column = 0;		
		curpos = "";
	}
	
	public void setInitialSpot(AllRooms room){//randomizes the initial starting point for the weapons on the board when the game is first run
		ArrayList<Point> p = room.getPoints();
		Random rand = new Random();
		int r = rand.nextInt(p.size()); 
		Point point = p.get(r);
		setPosition((int)point.getX(),(int)point.getY());
		setCurrentRoom(new GameBoard().getLocation(getRow(), getColumn()));
	}

	public void setPosition(int _row, int _col) {
		row = _row;
		column = _col;
	}
	
	public void setRow(int num){
		row = num;
	}
	
	public void setColumn(int spot){
		column = spot;
	}
	

	public int getRow() {

		return row;
	}

	public int getColumn() {
		return column;
	}

	@Override
	public String getImage() {
		return "";
	}

	public void setCurrentRoom(String s) {
		curpos = s;
	}

	public String getCurrentRoom() {
		return curpos;
	}
	
	public ImageIcon getImageIcon(){
		return null;
	}
	
}
	
	