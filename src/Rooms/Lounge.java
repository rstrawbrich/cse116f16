package Rooms;

import java.awt.Point;
import java.util.ArrayList;


public class Lounge extends AllRooms {

	public Lounge() {
		super();
	}

	@Override
	public String getName() {
		return "Lounge";
	}

	@Override
	public String getImage() {
		return "Lounge.PNG";
	}
	
	@Override
	public ArrayList<Point> getPoints(){
		ArrayList<Point> retVal = new ArrayList<Point>();
		
		Point p1 = new Point(0,16);
		retVal.add(p1);
		Point p2 = new Point(4,21);
		retVal.add(p2);
		Point p3 = new Point(2,18);
		retVal.add(p3);
		Point p4 = new Point(2,19);
		retVal.add(p4);
		Point p5 = new Point(4,16);
		retVal.add(p5);
		Point p6 = new Point(0,21);
		retVal.add(p6);
		Point p7 = new Point(2,17);
		retVal.add(p7);
		Point p8 = new Point(2,20);
		retVal.add(p8);
		Point p9 = new Point(1,21);
		retVal.add(p9);
		Point p10 = new Point(0,20);
		retVal.add(p10);
		Point p11 = new Point(3,17);
		retVal.add(p11);
		Point p12 = new Point(1,17);
		retVal.add(p12);
		
		return retVal;
	}
}
