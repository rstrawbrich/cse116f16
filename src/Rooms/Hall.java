package Rooms;

import java.awt.Point;
import java.util.ArrayList;

public class Hall extends AllRooms {

	public Hall() {
		super();
	}

	@Override
	public String getName() {
		return "Hall";
	}

	@Override
	public String getImage() {
		return "Hall.PNG";
	}
	
	@Override
	public ArrayList<Point> getPoints(){
		ArrayList<Point> retVal = new ArrayList<Point>();
		
		Point p1 = new Point(3,8);
		retVal.add(p1);
		Point p2 = new Point(0,13);
		retVal.add(p2);
		Point p3 = new Point(3,10);
		retVal.add(p3);
		Point p4 = new Point(5,9);
		retVal.add(p4);
		Point p5 = new Point(4,8);
		retVal.add(p5);
		Point p6 = new Point(1,13);
		retVal.add(p6);
		Point p7 = new Point(5,11);
		retVal.add(p7);
		Point p8 = new Point(1,10);
		retVal.add(p8);
		Point p9 = new Point(5,8);
		retVal.add(p9);
		Point p10 = new Point(5,13);
		retVal.add(p10);
		Point p11 = new Point(5,12);
		retVal.add(p11);
		Point p12 = new Point(3,12);
		retVal.add(p12);
		
		return retVal;
	}
	
}
