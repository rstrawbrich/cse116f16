package Rooms;

import java.awt.Point;
import java.util.ArrayList;

public class Conservatory extends AllRooms {

	public Conservatory() {
		super();
	}

	@Override
	public String getName() {
		return "Conservatory";
	}

	@Override
	public String getImage() {
		return "Conservatory.PNG";
	}
	
	@Override
	public ArrayList<Point> getPoints(){
		ArrayList<Point> retVal = new ArrayList<Point>();
		
		Point p1 = new Point(19,0);
		retVal.add(p1);
		Point p2 = new Point(20,0);
		retVal.add(p2);
		Point p3 = new Point(19,1);
		retVal.add(p3);
		Point p4 = new Point(20,1);
		retVal.add(p4);
		Point p5 = new Point(19,2);
		retVal.add(p5);
		Point p6 = new Point(20,2);
		retVal.add(p6);
		Point p7 = new Point(19,3);
		retVal.add(p7);
		Point p8 = new Point(20,3);
		retVal.add(p8);
		Point p9 = new Point(21,1);
		retVal.add(p9);
		Point p10 = new Point(19,4);
		retVal.add(p10);
		Point p11 = new Point(20,4);
		retVal.add(p11);
		Point p12 = new Point(21,4);
		retVal.add(p12);
		
		return retVal;
	}

}
