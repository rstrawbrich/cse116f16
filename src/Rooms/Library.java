package Rooms;

import java.awt.Point;
import java.util.ArrayList;


public class Library extends AllRooms {

	public Library() {
		super();
	}

	@Override
	public String getName() {
		return "Library";
	}


	@Override
	public String getImage() {
		return "Library.PNG";
	}
	
	@Override
	public ArrayList<Point> getPoints(){
		ArrayList<Point> retVal = new ArrayList<Point>();
		
		Point p1 = new Point(10,0);
		retVal.add(p1);
		Point p2 = new Point(9,0);
		retVal.add(p2);
		Point p3 = new Point(8,0);
		retVal.add(p3);
		Point p4 = new Point(8,1);
		retVal.add(p4);
		Point p5 = new Point(8,2);
		retVal.add(p5);
		Point p6 = new Point(8,3);
		retVal.add(p6);
		Point p7 = new Point(8,4);
		retVal.add(p7);
		Point p8 = new Point(9,4);
		retVal.add(p8);
		Point p9 = new Point(6,3);
		retVal.add(p9);
		Point p10 = new Point(7,2);
		retVal.add(p10);
		Point p11 = new Point(7,3);
		retVal.add(p11);
		Point p12 = new Point(7,4);
		retVal.add(p12);
		
		return retVal;
	}
}
