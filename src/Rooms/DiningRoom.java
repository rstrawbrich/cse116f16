package Rooms;

import java.awt.Point;
import java.util.ArrayList;


public class DiningRoom extends AllRooms {

	public DiningRoom() {
		super();
	}

	@Override
	public String getName() {
		return "DiningRoom";
	}

	@Override
	public String getImage() {
		return "DiningRoom.PNG";
	}

	@Override

	public ArrayList<Point> getPoints(){
		ArrayList<Point> retVal = new ArrayList<Point>();
		
		Point p1 = new Point(10,16);
		retVal.add(p1);
		Point p2 = new Point(11,16);
		retVal.add(p2);
		Point p3 = new Point(12,16);
		retVal.add(p3);
		Point p4 = new Point(13,16);
		retVal.add(p4);
		Point p5 = new Point(14,16);
		retVal.add(p5);
		Point p6 = new Point(15,16);
		retVal.add(p6);
		Point p7 = new Point(14,15);
		retVal.add(p7);
		Point p8 = new Point(15,15);
		retVal.add(p8);
		Point p9 = new Point(14,17);
		retVal.add(p9);
		Point p10 = new Point(15,17);
		retVal.add(p10);
		Point p11 = new Point(11,21);
		retVal.add(p11);
		Point p12 = new Point(12,21);
		retVal.add(p12);

		return retVal;
	}

}
