package Rooms;

import java.awt.Point;
import java.util.ArrayList;


public class Kitchen extends AllRooms {

	public Kitchen() {
		super();
	}

	@Override
	public String getName() {
		return "Kitchen";
	}

	@Override
	public String getImage() {
		return "Kitchen.PNG";
	}

	@Override
	public ArrayList<Point> getPoints(){
		ArrayList<Point> retVal = new ArrayList<Point>();
		
		Point p1 = new Point(17,17);
		retVal.add(p1);
		Point p2 = new Point(17,18);
		retVal.add(p2);
		Point p3 = new Point(17,19);
		retVal.add(p3);
		Point p4 = new Point(18,17);
		retVal.add(p4);
		Point p5 = new Point(18,16);
		retVal.add(p5);
		Point p6 = new Point(18,18);
		retVal.add(p6);
		Point p7 = new Point(19,16);
		retVal.add(p7);
		Point p8 = new Point(19,17);
		retVal.add(p8);
		Point p9 = new Point(19,19);
		retVal.add(p9);
		Point p10 = new Point(19,20);
		retVal.add(p10);
		Point p11 = new Point(21,18);
		retVal.add(p11);
		Point p12 = new Point(21,20);
		retVal.add(p12);
		
		return retVal;
	}

}
