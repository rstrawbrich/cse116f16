package Exceptions;

public class MustRollDiceFirstException extends Exception {//exception thrown in case the player tries to end their turn before rolling the dice

	public MustRollDiceFirstException() {
		super();
	}

}
