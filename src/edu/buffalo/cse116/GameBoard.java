package edu.buffalo.cse116;

public class GameBoard {
 

	private String[][] gameboard;

	public GameBoard() {
		gameboard = new String[22][22];
		setHallways();
		setRooms();
		setDoors();
	}

	// this starts the production of the game board as an 2-D array of string
	// values.

	public void setHallways() {
		for (int i = 0; i < 22; i++) {
			for (int j = 0; j < 22; j++) {
				gameboard[i][j] = "Hallway";
			}
		}
	}

	public void setRooms() {

		for (int i = 0; i < 6; i++) {
			for (int j = 8; j < 14; j++) {
				gameboard[i][j] = "Hall";
			}
		}
		for (int i = 0; i < 5; i++) {
			for (int j = 16; j < 22; j++) {
				gameboard[i][j] = "Lounge";
			}
		}

		for (int i = 8; i < 16; i++) {
			for (int j = 15; j < 22; j++) {
				gameboard[i][j] = "DiningRoom";
			}

		}
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 6; j++) {
				gameboard[i][j] = "Study";
			}
		}
		for (int i = 6; i < 11; i++) {
			for (int j = 0; j < 5; j++) {
				gameboard[i][j] = "Library";
			}
		}
		for (int i = 12; i < 17; i++) {
			for (int j = 0; j < 5; j++) {
				gameboard[i][j] = "BillardRoom";
			}
		}
		for (int i = 18; i < 22; i++) {
			for (int j = 0; j < 5; j++) {
				gameboard[i][j] = "Conservatory";
			}
		}
		for (int i = 14; i < 21; i++) {
			for (int j = 7; j < 13; j++) {
				gameboard[i][j] = "BallRoom";
			}
		}
		for(int i=7;i<13;i++){
			for(int j=8;j<13;j++){
				gameboard[i][j] = "MysterySpot";
			}
		}
		for (int i = 17; i < 22; i++) {
			for (int j = 16; j < 22; j++) {
				gameboard[i][j] = "Kitchen";
			}
		}
	}

	public String getLocation(int i, int j) {// lets player know that they are
												// in a room or hallway at that
												// moment
		return gameboard[i][j];
	}

	public void setDoors() {
		gameboard[4][5] = "doorspace";
		gameboard[8][5] = "doorspace";
		gameboard[13][5] = "doorspace";
		gameboard[18][5] = "doorspace";
		gameboard[13][10] = "doorspace";
		gameboard[6][10] = "doorspace";
		gameboard[5][17] = "doorspace";
		gameboard[7][16] = "doorspace";
		gameboard[17][15] = "doorspace";
	}
}
