package edu.buffalo.cse116;

import java.util.Random;

//Method generates a number between 2 and 12

public class DiceRoll {

	public int Dice() {
		Random rand = new Random();
		int n = rand.nextInt(6) + 1;
		return n;
	}

}
