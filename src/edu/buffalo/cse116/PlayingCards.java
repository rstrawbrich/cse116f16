package edu.buffalo.cse116;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import Characters.*;
import Exceptions.*;
import Observer.Observer;
import PlayersOfGame.AllPlayers;
import Rooms.*;
import Weapons.*;

//This class creates an array of players, assigns cards to the three set players, and takes care of how suggestions are handled.

public class PlayingCards {
	public static int MAX_DICEROLL = 1;
	private int _size, indexofcurrentplayer,suggestiontime;
	private ArrayList<AllPlayers> arrayofplayers;
	private GameBoard gameboard;
	private ArrayList<String> room;
	protected ArrayList<AllCards> mystery;
	private String target, playerhasthecard;
	private AllPlayers currentplayer;
	private ArrayList<AllWeapons> allweapons;
	private ArrayList<Observer> observer;
	private ArrayList<AllCards> weapons,characters,rooms;
	private ArrayList<AllCards> distributiondeck,exposedcards;;
	private ArrayList<AllCards> _handforplayer1,_handforplayer2,_handforplayer3,_handforplayer4,_handforplayer5,_handforplayer6;


	public PlayingCards() {
		
		observer = new ArrayList<Observer>();
	
		gameboard = new GameBoard();
		
		suggestiontime = 0;
		playerhasthecard = "";
		exposedcards = new ArrayList<AllCards>();
		
		initializeAllCards();
		
		distributeCardsToPlayersHand();
			
		initializePlayers();
		
		initializeWeapons();
		
		setRoom();

		getCurrentPlayer();
		
	}

	public void notifyObservers(){
		for(Observer ob : observer){
			ob.update();
		}
	}
	public void addObservers(Observer ob){
		observer.add(ob);
	}
	
	public void initializeAllCards(){//creates all the arrays and lists of cards for use throughout the game
		weapons = new ArrayList<AllCards>();
		characters = new ArrayList<AllCards>();
		rooms = new ArrayList<AllCards>();
		AllCards[] weapon = new AllCards[] { new Knife(), new Wrench(), new CandleStick(), new Revolver(), new Rope(),
				new LeadPipe() };
		AllCards[] character = new AllCards[] { new MsScarlet(), new MrGreen(), new ColMustard(), new MsPeacock(),
				new ProfPlum(), new MrsWhite() };
		AllCards[] room = new AllCards[] { new Conservatory(), new Lounge(), new Hall(), new Study(), new Kitchen(),
				new BillardsRoom(), new DiningRoom(), new Ballroom(), new Library() };
		weapons.addAll(Arrays.asList(weapon));
		characters.addAll(Arrays.asList(character));
		rooms.addAll(Arrays.asList(room));
	}
	
	public void initializeWeapons(){
		allweapons = new ArrayList<AllWeapons>();
		allweapons.addAll(Arrays.asList(new AllWeapons[] { new Knife(), new Wrench(), new CandleStick(), new Revolver(),
				new Rope(), new LeadPipe() }));
	}
	
	public void initializePlayers(){//adds the players into a list in the same order every time and assigns the player a hand of cards
		
		AllPlayers MissScarlet = new AllPlayers(_handforplayer1, 0);
		AllPlayers ProfPlum = new AllPlayers(_handforplayer2, 1 );
		AllPlayers MrGreen = new AllPlayers(_handforplayer3, 2);
		AllPlayers MrsWhite = new AllPlayers(_handforplayer4, 3);
		AllPlayers MrsPeacock = new AllPlayers(_handforplayer5, 4);
		AllPlayers ColMustard = new AllPlayers(_handforplayer6, 5);
		
		arrayofplayers = new ArrayList<AllPlayers>();
		arrayofplayers.add(MissScarlet);
		arrayofplayers.add(ProfPlum);
		arrayofplayers.add(MrGreen);
		arrayofplayers.add(MrsWhite);
		arrayofplayers.add(MrsPeacock);
		arrayofplayers.add(ColMustard);
		
		indexofcurrentplayer = 0;
	}

	public void endPlayersTurn() throws MustRollDiceFirstException {//used  in the end turn handler to end the current players turn and move to the next player, throws exception if the dice isn't rolled first
		if (currentplayer.getDiceRollTimes() == MAX_DICEROLL) {
			currentplayer.resetTheNumberofTimesForDiceRoll();
			currentplayer.resetTheSteps();
			currentplayer.resetPreviousMove();
			suggestiontime = 0;
			indexofcurrentplayer += 1;
			indexofcurrentplayer = indexofcurrentplayer % arrayofplayers.size();
			currentplayer = arrayofplayers.get(indexofcurrentplayer);
			if(currentplayer.getPlayerState()==false){
				currentplayer.setTheNumberofTimesForDiceRoll();
				endPlayersTurn();
				notifyObservers();
				
			}else {
				currentplayer = arrayofplayers.get(indexofcurrentplayer);
				notifyObservers();
			} 
		} 
		else{throw new MustRollDiceFirstException();}
	}

	public void exitRoom() throws OutOfStepsException{//throws an exception if the player has entered a room and exits  the same room in the same turn and handles what to do with the players number of steps when they exit the room
		if(currentplayer.getAvailableSpacesToTake()!=0){
			currentplayer.exitRoomOption();
			currentplayer.resetPreviousMove();
			notifyObservers();
		} else {
			throw new OutOfStepsException();
		}
	}
	
	public void shuffleCardsforHand() {//shuffles the cards that are used to distribute to the players after three have been removed first
		shuffleAndRemoveThreeCards();
		distributiondeck = new ArrayList<AllCards>();
		distributiondeck.addAll(characters);
		distributiondeck.addAll(rooms);
		distributiondeck.addAll(weapons);
		Collections.shuffle(distributiondeck);

	}

	public void distributeCardsToPlayersHand() {//distributes the cards to the players as if you were going in circular motion around a table
		shuffleCardsforHand();
		_handforplayer1 = new ArrayList<AllCards>();
		_handforplayer2 = new ArrayList<AllCards>();
		_handforplayer3 = new ArrayList<AllCards>();
		_handforplayer4 = new ArrayList<AllCards>();
		_handforplayer5 = new ArrayList<AllCards>();
		_handforplayer6 = new ArrayList<AllCards>();
		_handforplayer1.add(distributiondeck.get(0));
		_handforplayer2.add(distributiondeck.get(1));
		_handforplayer3.add(distributiondeck.get(2));
		_handforplayer4.add(distributiondeck.get(3));
		_handforplayer5.add(distributiondeck.get(4));
		_handforplayer6.add(distributiondeck.get(5));
		_handforplayer1.add(distributiondeck.get(6));
		_handforplayer2.add(distributiondeck.get(7));
		_handforplayer3.add(distributiondeck.get(8));
		_handforplayer4.add(distributiondeck.get(9));
		_handforplayer5.add(distributiondeck.get(10));
		_handforplayer6.add(distributiondeck.get(11));
		_handforplayer1.add(distributiondeck.get(12));
		_handforplayer2.add(distributiondeck.get(13));
		_handforplayer3.add(distributiondeck.get(14));
		_handforplayer4.add(distributiondeck.get(15));
		_handforplayer5.add(distributiondeck.get(16));
		_handforplayer6.add(distributiondeck.get(17));
	}

	public ArrayList<AllCards> shuffleAndRemoveThreeCards() {//shuffles all the cards in the game, then removes three, and stores them in a list for use in the final accusation of the game
		
		mystery = new ArrayList<AllCards>();
		Collections.shuffle(rooms);
		Collections.shuffle(characters);
		Collections.shuffle(weapons);
		mystery.add(characters.get(0));
		mystery.add(weapons.get(0));
		mystery.add(rooms.get(0));
		rooms.remove(0);
		characters.remove(0);
		weapons.remove(0);
		System.out.println(mystery.get(0).getName() + " " + mystery.get(1).getName() + " " + mystery.get(2).getName());
		return mystery;
	}

	public void setRoom() {// adds all the rooms to an array list to use whether
							// the player has entered a room thats available
		room = new ArrayList<String>();
		room.add("Study");
		room.add("Conservatory");
		room.add("Hall");
		room.add("BillardRoom");
		room.add("Kitchen");
		room.add("BallRoom");
		room.add("DiningRoom");
		room.add("Library");
		room.add("Lounge");
	}
	public AllPlayers getCurrentPlayer() {//retrieves the current player from the array of players
		currentplayer = arrayofplayers.get(indexofcurrentplayer);
		return currentplayer;
	}

	public int size() {
		return _size;
	}

	public List<AllPlayers> getAllPlayers() {// accessor method for the
												// suggestion rotation method
												// below.
		return arrayofplayers;
	}

	public ArrayList<AllCards> getExposedCards() {
		return exposedcards;
	}

	public String getPlayerhasthecard() {
		return playerhasthecard;
	}

	public Point generateRandomPoints(ArrayList<Point> al){//generates a random point for placement in the rooms when a player enters a room and also helps with moving the pieces(characters and weapons) on the board so that when something is moved to a room as a result of the suggestion that the pieces have their own distinct location in the room
		Random rand = new Random();
		int r = rand.nextInt(al.size()); 	
		return al.get(r);
	}

	
	public void checkForCollisionAndShift(AllPlayers p){//checks for general collision for the computer tries to place a character and a weapon in the same spot as a result of moving the pieces when a suggestion is made in the room
		AllRooms[] rooms = new AllRooms[]{new Conservatory(), new Lounge(), new Hall(), new Study(), new Kitchen(), new BillardsRoom(), new DiningRoom(), new Ballroom(), new Library()};
		
		AllRooms currentroom = null;
		for (AllRooms r : rooms) {
			if (r.getName().equals(currentplayer.currentPosition())) {
				currentroom = r;
			}
		}
		boolean roommatch = p.currentPosition().equals(currentroom.getName());
		boolean iscurrentplayer = p.getcharacterName().equals(currentplayer.getcharacterName());
		
		if(!roommatch||iscurrentplayer){
			int index = p.getIndex();
			ArrayList<Point> points = currentroom.getPoints();
			Point point = generateRandomPoints(points);   
			int row = (int) point.getX();
			int column = (int) point.getY();
			boolean playercollision = false;
			boolean weaponcollision = false;
			for(int i = index+1;index!=i%arrayofplayers.size();i++){
				int modular = i%arrayofplayers.size();
				boolean collision = (arrayofplayers.get(modular).currentRow()==row&&arrayofplayers.get(modular).currentColumn()==column);
				playercollision = playercollision || collision;
			}
			for (AllWeapons w : allweapons) {
				boolean collision = (w.getRow() == row && w.getColumn() == column);
				weaponcollision = weaponcollision || collision;
			}
			if (playercollision || weaponcollision) {
				checkForCollisionAndShift(p);
			} else {
				p.setRow(row);
				p.setColumn(column);
				p.setCurrentPosition(currentroom.getName());
			}
		}
	}


	public void weaponcollisionandshift(AllWeapons weap){//shifts the weapons in the room to their own places if the placement due to moving the piece as a result of suggestion conflicts with a standing location of another weapon and finds a distinct place for each weapon
		AllRooms[] rooms = new AllRooms[]{new Conservatory(), new Lounge(), new Hall(), new Study(), new Kitchen(), new BillardsRoom(), new DiningRoom(), new Ballroom(), new Library()};

		AllRooms currentroom = null;
		for (AllRooms r : rooms) {
			if (r.getName().equals(currentplayer.currentPosition())) {
				currentroom = r;
			}
		}

		if(!weap.getCurrentRoom().equals(currentroom.getName())){
			ArrayList<Point> points = currentroom.getPoints();
			Point point = generateRandomPoints(points);   
			int row = (int) point.getX();
			int column = (int) point.getY();

			boolean playercollision = false;
			boolean weaponcollision = false;
			for (AllPlayers p : arrayofplayers) {
				boolean collision = (p.currentRow() == row && p.currentColumn() == column);
				playercollision = playercollision || collision;
			}
			for (AllWeapons w : allweapons) {
				boolean collision = (w.getRow() == row && w.getColumn() == column);
				weaponcollision = weaponcollision || collision;
			}
			if (playercollision || weaponcollision) {
				weaponcollisionandshift(weap);
			} else {
				weap.setPosition(row, column);
				weap.setCurrentRoom(currentroom.getName());
			}
		}
	}

	//Rotates between the players based on the suggestion made.
	public boolean suggestionRotation(AllCharacters character,AllWeapons weapon, AllRooms room){
		if(currentplayer.currentPosition().equals(room)){
			return false;
		}
		exposedcards.clear();
		for (AllPlayers p : arrayofplayers) {
			if (character.getName().equals(p.getcharacterName())) {
				checkForCollisionAndShift(p);
			}
		}
		for (AllWeapons w : allweapons) {
			if (weapon.getName().equals(w.getName())) {
				weaponcollisionandshift(weapon);
			}
		}
		
		for (int i = indexofcurrentplayer + 1; indexofcurrentplayer != i % arrayofplayers.size(); i++) {
			int modular = i % arrayofplayers.size();
			if (arrayofplayers.get(modular).suggestion(character, weapon, room)) {
				AllPlayers player = arrayofplayers.get(modular);
				playerhasthecard = player.getcharacterName();
				for (AllCards c : player.getCards()) {
					if (c.getName() == character.getName()) {
						exposedcards.add(c);
					}
					if (c.getName() == weapon.getName()) {
						exposedcards.add(c);
					}
					if (c.getName() == room.getName()) {
						exposedcards.add(c);
					}
				}
				suggestiontime = 1;
				return true;
			} 
		}
		if (currentplayer.suggestion(character, weapon, room)) {
			playerhasthecard = currentplayer.getcharacterName();
			for (AllCards c : currentplayer.getCards()) {
				if (c.getName() == character.getName()) {
					exposedcards.add(c);
				}
				if (c.getName() == weapon.getName()) {
					exposedcards.add(c);
				}
				if (c.getName() == room.getName()) {
					exposedcards.add(c);
				}
			}
			suggestiontime = 1;
			return true;
		}
		suggestiontime = 1;
		return false;
	}
	public boolean makeAccusation(AllCharacters character, AllWeapons weapon, AllRooms room){//decides whether the accusation made matches with the cards in the mystery cards list set aside at the beginning of the game
		boolean retVal;
		retVal = mystery.get(0).match(character);
		retVal = retVal && mystery.get(1).match(weapon);
		retVal = retVal && room.match(mystery.get(2));
		return retVal; 
	}	
	
	public boolean moveUp() throws OutOfStepsException, CollisionException,OppositeMoveException,MoveThroughWallException, IllegalMoveException{//this is for upward vertical movement of player
		if(currentplayer.getPreviousMove().equals("down")){
			throw new OppositeMoveException();
		}
		for (int i = currentplayer.getIndex() + 1; i < arrayofplayers.size(); i++) {
			if (arrayofplayers.get(i).currentRow() == currentplayer.currentRow() - 1
					&& arrayofplayers.get(i).currentColumn() == currentplayer.currentColumn()) {
				throw new CollisionException();
			}
		}
		for (int i = 0; i < currentplayer.getIndex(); i++) {
			if (arrayofplayers.get(i).currentRow() == currentplayer.currentRow() - 1
					&& arrayofplayers.get(i).currentColumn() == currentplayer.currentColumn()) {
				throw new CollisionException();
			}
		}

		target = gameboard.getLocation(currentplayer.currentRow()-1,currentplayer.currentColumn());
		if(target.equals("MysterySpot")){
			throw new IllegalMoveException();
		}
		if(currentplayer.getAvailableSpacesToTake()>0){
			if(room.contains(target)){
				if(currentplayer.currentPosition().equals("doorspace")){
				currentplayer.setCurrentPosition(target);
				currentplayer.moveUp();
				checkForCollisionAndShift(currentplayer);
				currentplayer.setSteps(0);
				notifyObservers();
				return true;
				} else {
					throw new MoveThroughWallException();
				}
			}
			currentplayer.setCurrentPosition(target);
			currentplayer.moveUp();
			notifyObservers();
			return true;
		}
		throw new OutOfStepsException();
	}

	
	public boolean moveDown() throws OutOfStepsException,CollisionException,OppositeMoveException,MoveThroughWallException, IllegalMoveException{ //for downward vertical movement of player

		if(currentplayer.getPreviousMove().equals("up")){

			throw new OppositeMoveException();
		} // since we can not move backward
		for (int i = currentplayer.getIndex() + 1; i < arrayofplayers.size(); i++) {
			if (arrayofplayers.get(i).currentRow() == currentplayer.currentRow() + 1
					&& arrayofplayers.get(i).currentColumn() == currentplayer.currentColumn()) {
				throw new CollisionException();
			}
		} // check if the space got occupied by other players
		for (int i = 0; i < currentplayer.getIndex(); i++) {
			if (arrayofplayers.get(i).currentRow() == currentplayer.currentRow() + 1
					&& arrayofplayers.get(i).currentColumn() == currentplayer.currentColumn()) {
				throw new CollisionException();
			}

		}// check if the space got occupied by other players
		if(currentplayer.getAvailableSpacesToTake()>0){
			target = gameboard.getLocation(currentplayer.currentRow()+1,currentplayer.currentColumn()); // the target place
			if(target.equals("MysterySpot")){
				throw new IllegalMoveException();
			}
			if(room.contains(target)){//if trying to enter a room
				if(currentplayer.currentPosition().equals("doorspace")){ 
				currentplayer.setCurrentPosition(target);  // set the current position as target
				currentplayer.moveDown(); // see the moveDown method in player class
				checkForCollisionAndShift(currentplayer);
				currentplayer.setSteps(0);//steps become zero when entering a room
				notifyObservers();
				return true;
				} else {
					throw new MoveThroughWallException();
				}
			}
			currentplayer.setCurrentPosition(target); //when moving through the hallway
			currentplayer.moveDown();
			notifyObservers();
			return true;
		}
		throw new OutOfStepsException();
	}
	
	public boolean moveLeft() throws OutOfStepsException, CollisionException,OppositeMoveException,MoveThroughWallException, IllegalMoveException{//for leftward horizontal movement of player
		if(currentplayer.getPreviousMove().equals("right")){
			throw new OppositeMoveException();
		} // since we can not move backward

		for (int i = currentplayer.getIndex() + 1; i < arrayofplayers.size(); i++) {
			if (arrayofplayers.get(i).currentColumn() == currentplayer.currentColumn() - 1
					&& arrayofplayers.get(i).currentRow() == currentplayer.currentRow()) {
				throw new CollisionException();
			}
		} // check if the space got occupied by other players
		for (int i = 0; i < currentplayer.getIndex(); i++) {
			if (arrayofplayers.get(i).currentColumn() == currentplayer.currentColumn() - 1
					&& arrayofplayers.get(i).currentRow() == currentplayer.currentRow()) {
				throw new CollisionException();
			}

		}// check if the space got occupied by other players
		if(currentplayer.getAvailableSpacesToTake()>0){
			target = gameboard.getLocation(currentplayer.currentRow(),currentplayer.currentColumn()-1); // the target place
			if(target.equals("MysterySpot")){
				throw new IllegalMoveException();
			}
			if(room.contains(target)){//if trying to enter a room
				if(currentplayer.currentPosition().equals("doorspace")){ 
				currentplayer.setCurrentPosition(target);  // set the current position as target
				currentplayer.moveLeft(); // see the moveDown method in player class
				checkForCollisionAndShift(currentplayer);
				currentplayer.setSteps(0);//steps become zero when entering a room
				notifyObservers();
				return true;
				} else {
					throw new MoveThroughWallException();
				}
			}
			currentplayer.setCurrentPosition(target); // when traversing the
														// hallway
			currentplayer.moveLeft();
			notifyObservers();
			return true;
		}
		throw new OutOfStepsException();
	}

	
	public boolean moveRight() throws OutOfStepsException, CollisionException,OppositeMoveException,MoveThroughWallException, IllegalMoveException{ //for rightward vertical movement of player
		if(currentplayer.getPreviousMove().equals("left")){
			throw new OppositeMoveException();
		} // since we can not move backward

		for (int i = currentplayer.getIndex() + 1; i < arrayofplayers.size(); i++) {
			if (arrayofplayers.get(i).currentColumn() == currentplayer.currentColumn() + 1
					&& arrayofplayers.get(i).currentRow() == currentplayer.currentRow()) {
				throw new CollisionException();
			}
		} // check if the space got occupied by other players
		for (int i = 0; i < currentplayer.getIndex(); i++) {
			if (arrayofplayers.get(i).currentColumn() == currentplayer.currentColumn() + 1
					&& arrayofplayers.get(i).currentRow() == currentplayer.currentRow()) {
				throw new CollisionException();
			}

		}// check if the space got occupied by other players
		if(currentplayer.getAvailableSpacesToTake()>0){
			target = gameboard.getLocation(currentplayer.currentRow(),currentplayer.currentColumn()+1); // the target place
			if(target.equals("MysterySpot")){
				throw new IllegalMoveException();
			}
			if(room.contains(target)){//if trying to enter a room 
				if(currentplayer.currentPosition().equals("doorspace")){ 
				currentplayer.setCurrentPosition(target);  // set the current position as target
				currentplayer.moveRight(); // see the moveDown method in player class
				checkForCollisionAndShift(currentplayer);
				currentplayer.setSteps(0);//if entering a room, steps become zero
				notifyObservers();
				return true;
				} else {
					throw new MoveThroughWallException();
				}
			}
			currentplayer.setCurrentPosition(target); // if traversing through
														// the hallway
			currentplayer.moveRight();
			notifyObservers();
			return true;
		}
		throw new OutOfStepsException();
	}

	
	public boolean moveUpWithAllSteps(AllPlayers player) throws OutOfStepsException, CollisionException, OppositeMoveException, MoveThroughWallException, IllegalMoveException{//loops through whether the player can move up with all the available steps that they have currently
		for(int i = currentplayer.getAvailableSpacesToTake(); i!=0;i--){

			moveUp();
		}
		return true;
	}

	
	public boolean moveDownWithAllSteps(AllPlayers player) throws OutOfStepsException, CollisionException, OppositeMoveException, MoveThroughWallException, IllegalMoveException{ //loops through whether the player can move down with all the available steps that they have currently
		for(int i = currentplayer.getAvailableSpacesToTake(); i!=0;i--){

			moveDown();
		}
		return true;
	}

	public boolean moveLeftWithAllSteps(AllPlayers player) throws OutOfStepsException, CollisionException, OppositeMoveException, MoveThroughWallException, IllegalMoveException{ //loops through whether the player can move left with all the avaialable steps that they have currently
		for(int i = currentplayer.getAvailableSpacesToTake(); i!=0;i--){
			moveLeft();
		}
		return true;
	}

	
	public boolean moveRightWithAllSteps(AllPlayers player) throws OutOfStepsException, CollisionException, OppositeMoveException, MoveThroughWallException, IllegalMoveException{ //loops through whether the player can move right with all the available steps that they have currently
		for(int i = currentplayer.getAvailableSpacesToTake(); i!=0;i--){
			moveRight();
		}
		return true;
	}

	public boolean moveDiagonally() {// says that the player cannot move
										// diagonally ever
		return false;
	}

	public boolean secretpassageway(){// says that the player can move through a secret passageway at the beginning of their turn to traverse the appropriate corners of the game board
		if(currentplayer.getAvailableSpacesToTake()>0){
			if(currentplayer.currentPosition().equals("Study")){
			currentplayer.setCurrentPosition("Kitchen");
			currentplayer.setSteps(0);
			checkForCollisionAndShift(currentplayer);
			notifyObservers();
			return true;
			}
			if(currentplayer.currentPosition().equals("Kitchen")){
			currentplayer.setCurrentPosition("Study");
			currentplayer.setSteps(0);
			checkForCollisionAndShift(currentplayer);
			notifyObservers();
			return true;
			}
			if(currentplayer.currentPosition().equals("Conservatory")){
			currentplayer.setCurrentPosition("Lounge");
			currentplayer.setSteps(0);
			checkForCollisionAndShift(currentplayer);
			notifyObservers();
			return true;
			}
			if(currentplayer.currentPosition().equals("Lounge")){
			currentplayer.setCurrentPosition("Conservatory");
			currentplayer.setSteps(0);
			checkForCollisionAndShift(currentplayer);
			notifyObservers();
			return true;
			}
		}
		return false;
	}

	public int getIndexOfCurrentPlayer() {
		return indexofcurrentplayer;
	}

	public ArrayList<AllWeapons> getWeapons() {
		return allweapons;
	}

	public ArrayList<AllCards> getWeaponCards() {

		return weapons;
	}

	public ArrayList<AllCards> getCharacterCards() {
		return characters;
	}

	public ArrayList<AllCards> getRoomCards() {
		return rooms;
	}
	
	public boolean alreadyMakeSuggestion(){
		return suggestiontime==1;
	}
}
