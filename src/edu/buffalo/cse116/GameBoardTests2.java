package edu.buffalo.cse116;

import static org.junit.Assert.*;

import org.junit.Test;

import Exceptions.MustRollDiceFirstException;
import PlayersOfGame.AllPlayers;

public class GameBoardTests2 {

	@Test
	public void testOrderOfPlayers() {
		PlayingCards pcs = new PlayingCards();
		assertEquals("MsScarlet", pcs.getAllPlayers().get(0).getcharacterName());
		assertEquals("ProfPlum", pcs.getAllPlayers().get(1).getcharacterName());
		assertEquals("MrGreen", pcs.getAllPlayers().get(2).getcharacterName());
		assertEquals("MrsWhite", pcs.getAllPlayers().get(3).getcharacterName());
		assertEquals("MsPeacock", pcs.getAllPlayers().get(4).getcharacterName());
		assertEquals("ColMustard", pcs.getAllPlayers().get(5).getcharacterName());
	}

	@Test
	public void testEndTurn() throws MustRollDiceFirstException {
		PlayingCards pcs = new PlayingCards();
		AllPlayers player1 = pcs.getCurrentPlayer();
		int currentplayer = pcs.getIndexOfCurrentPlayer();
		pcs.endPlayersTurn();
		AllPlayers player2 = pcs.getCurrentPlayer();
		int currentplayer2 = pcs.getIndexOfCurrentPlayer();
		pcs.endPlayersTurn();

		assertTrue("" + player1.getcharacterName(), player1.getcharacterName().equals("MsScarlet"));
		assertTrue("" + currentplayer, currentplayer == 0);
		assertTrue("" + player2.getcharacterName(), player2.getcharacterName().equals("ProfPlum"));
		assertTrue("" + currentplayer, currentplayer2 == 1);
	}

}
