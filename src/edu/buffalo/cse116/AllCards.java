package edu.buffalo.cse116;

public abstract class AllCards {

	public AllCards() {
	}

	public boolean match(AllCards card) { // this method allows for the correct
											// comparison of the character,
											// weapon, and room classes.
		return getName().equals(card.getName());
	}

	public String getName() {// accessor method for the match method above.
		return "Name";
	}

	public abstract String getImage();

}
