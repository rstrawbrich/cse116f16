package edu.buffalo.cse116;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.MatteBorder;
import ButtonHandlers.*;
import Observer.Observer;
import PlayersOfGame.AllPlayers;
import Weapons.AllWeapons;



//sets up and constructs the GUI and all its elements( its left, middle, and right panels, including all buttons, player pieces, and weapon icons)
public class GUI implements Observer{
	private JFrame window;
	private JPanel boardpanel, commandpanel,movecommandpanel,actioncommandpanel,mainpanel,botmainpanel,playerpanel,numberofsteps;
	private JButton[][] jbut;
	private PlayingCards playingcard;
	private ArrayList<JLabel> al ;
	public static final String[] characters = {"MrGreen", "MsPeacock", "ProfPlum", "MsScarlet", "MrsWhite", "ColMustart"};
	public static final String[] weapons = {"leadpipe", "revolver", "wrench", "candlestick", "knife", "rope"};
	public static final String[] rooms = {"Hall", "Ballroom", "Diningroom", "Study", "Lounge", "Conservatory", "Billardroom", "Library", "Kitchen"};
	
	
	public GUI(PlayingCards ps){
		
		playingcard = ps;    
		
		ps.addObservers(this);   
		
		initializingMainPanel();   
		
		JOptionPane.showMessageDialog(null, "Welcome to Clue! Have Fun!");      
		
		JOptionPane.showMessageDialog(null, playingcard.getCurrentPlayer().getcharacterName()+"'s turn.");
			    
	}
	
	private void initializingMainPanel(){
		window = new JFrame();
		mainpanel = new JPanel();
		mainpanel.setOpaque(false);
		
		initializingBotMainPanel();
				
		update();
		
		window.setContentPane(mainpanel);
		window.setVisible(true);
		window.setResizable(false);
//		window.setLocationRelativeTo(null);
		window.setSize(new Dimension(1850,980));
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				
	} 
	
	private void initializingBotMainPanel(){
		botmainpanel = new JPanel();
		botmainpanel.setOpaque(false);
		botmainpanel.setLayout(new BoxLayout(botmainpanel,BoxLayout.X_AXIS));
		
		initializingPlayerBoard();
		initializingBoardPanel();
		initializingCommandPanel();
		
		mainpanel.add(botmainpanel);
		
	}
	
	private void initializingPlayerBoard(){
		playerpanel = new JPanel();
		playerpanel.setOpaque(false);
		playerpanel.setPreferredSize(new Dimension(200,950));
		playerpanel.setLayout(new GridLayout(6,2));	
		
		botmainpanel.add(playerpanel);
		
	}

	public void setPlayerBoard(){//sets player starting position of their characters
		playerpanel.removeAll();
		al = new ArrayList<JLabel>();
		JLabel jb1 = new JLabel(new ImageIcon(getClass().getResource("/ButtonHandlers/MsScarlet.PNG")));
		al.add(jb1);
		playerpanel.add(jb1);
		JLabel jbc1 = new JLabel();
		jbc1.setToolTipText(""+jbc1.getPreferredSize().getHeight());
		jbc1.setOpaque(true);
		jbc1.setBackground(Color.RED);
		playerpanel.add(jbc1);
		
		JLabel jb2 = new JLabel(new ImageIcon(getClass().getResource("/ButtonHandlers/ProfPlum.PNG")));
		al.add(jb2);
		playerpanel.add(jb2);
		JLabel jbc2 = new JLabel();
		jbc2.setOpaque(true);
		jbc2.setBackground(Color.MAGENTA);
		playerpanel.add(jbc2);
		
		JLabel jb3 = new JLabel(new ImageIcon(getClass().getResource("/ButtonHandlers/MrGreen.PNG")));
		al.add(jb3);
		playerpanel.add(jb3);
		JLabel jbc3 = new JLabel();
		jbc3.setOpaque(true);
		jbc3.setBackground(Color.GREEN);
		playerpanel.add(jbc3);
		
		JLabel jb4 = new JLabel(new ImageIcon(getClass().getResource("/ButtonHandlers/MrsWhite.PNG")));
		al.add(jb4);
		playerpanel.add(jb4);
		JLabel jbc4 = new JLabel();
		jbc4.setOpaque(true);
		jbc4.setBackground(Color.WHITE);
		playerpanel.add(jbc4);
		
		JLabel jb5 = new JLabel(new ImageIcon(getClass().getResource("/ButtonHandlers/MsPeacock.PNG")));
		al.add(jb5);
		playerpanel.add(jb5);
		JLabel jbc5 = new JLabel();
		jbc5.setOpaque(true);
		jbc5.setBackground(Color.BLUE);
		playerpanel.add(jbc5);
		
		JLabel jb6 = new JLabel(new ImageIcon(getClass().getResource("/ButtonHandlers/ColMustard.PNG")));
		al.add(jb6);
		playerpanel.add(jb6);
		JLabel jbc6 = new JLabel();
		jbc6.setOpaque(true);
		jbc6.setBackground(Color.YELLOW);
		playerpanel.add(jbc6);
		
		List<AllPlayers> players = playingcard.getAllPlayers();
		for(int i = 0;i<al.size()&&i<players.size();i++){
			boolean states = players.get(i).getPlayerState();
			al.get(i).setEnabled(states);
		}
		
	}
	
	
	private void initializingBoardPanel(){
		Image gameboard = new ImageIcon(getClass().getResource("GameBoard.PNG")).getImage();
		
		boardpanel = new JPanel(){
			@Override
			public void paintComponent(Graphics g){
				super.paintComponents(g);
				g.drawImage(gameboard,0,0, null);
			}
		};	
		
		boardpanel.setPreferredSize(new Dimension(1400,900));
		boardpanel.setLayout(new GridLayout(22,22,0,0));
		boardpanel.setOpaque(false);	
		
		botmainpanel.add(boardpanel);
		
	}
	
	public void addButtonsToBoardPanel(){//adds and displays the spaces(buttons) that the player can move through in the game board
		boardpanel.removeAll();
		jbut = new JButton[22][22];
		
		for(int i=0;i<22;i++){
			for(int j=0;j<22;j++){
				jbut[i][j] = new JButton();
				jbut[i][j].setBorderPainted(false);
				jbut[i][j].setOpaque(false);
				jbut[i][j].setContentAreaFilled(false);
				jbut[i][j].setToolTipText(""+i+" and "+j);
				boardpanel.add(jbut[i][j]);
			}
		}
		
		setPlayers();
		setWeapons();

		window.repaint();
	}
	
	public void addButtonsToTheActionCommandPanel(){//adds and displays all the buttons for the player to use in the game in the action command panel
		actioncommandpanel.removeAll();
		
		JButton takesecretpassage = new JButton(new ImageIcon(getClass().getResource("SecretPassage.PNG")));
		takesecretpassage.setOpaque(false);
		takesecretpassage.setContentAreaFilled(false);
		takesecretpassage.setBorderPainted(false);
		
		JButton makesuggestion = new JButton();
		makesuggestion.setIcon(new ImageIcon(getClass().getResource("suggestion.PNG")));
		makesuggestion.setOpaque(false);
		makesuggestion.setContentAreaFilled(false);
		makesuggestion.setBorderPainted(false);
		
		JButton endturn = new JButton(new ImageIcon(getClass().getResource("End.PNG")));
		endturn.setOpaque(false);
		endturn.setContentAreaFilled(false);
		endturn.setBorderPainted(false);
		
		JButton diceroll = new JButton(new ImageIcon(getClass().getResource("DiceRoll.PNG")));
		diceroll.setOpaque(false);
		diceroll.setContentAreaFilled(false);
		diceroll.setBorderPainted(false);
		
		JButton showcards = new JButton(new ImageIcon(getClass().getResource("Hand.PNG")));
		showcards.setOpaque(false);
		showcards.setContentAreaFilled(false);
		showcards.setBorderPainted(false);

		JButton exitroom = new JButton(new ImageIcon(getClass().getResource("Exit.PNG")));
		exitroom.setOpaque(false);
		exitroom.setContentAreaFilled(false);
		exitroom.setBorderPainted(false);
		
		JButton accusation = new JButton(new ImageIcon(getClass().getResource("accusation.PNG")));
		accusation.setOpaque(false);
		accusation.setContentAreaFilled(false);
		accusation.setBorderPainted(false);
		
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		panel.setOpaque(false);
		
		takesecretpassage.addActionListener(new SecretPassageHandler(playingcard, this));	
		showcards.addActionListener(new ShowCardsHandler(playingcard));
		makesuggestion.addActionListener(new SuggestionHandler(playingcard,this));
		endturn.addActionListener(new EndTurnHandler(playingcard));
		diceroll.addActionListener(new DiceRollHandler(playingcard,this));
		exitroom.addActionListener(new ExitRoomHandler(playingcard));
		accusation.addActionListener(new MakeAccusationHandler(playingcard,this));
		
		
		panel.add(diceroll,BorderLayout.WEST);
		panel.add(endturn,BorderLayout.EAST);
		actioncommandpanel.add(showcards);
		actioncommandpanel.add(exitroom);
		actioncommandpanel.add(takesecretpassage);
		actioncommandpanel.add(makesuggestion);
		actioncommandpanel.add(accusation);
		actioncommandpanel.add(panel);

		window.repaint();
	}

	public void setPlayers() {//sets the position of all the players character pieces throughout the game board
		List<AllPlayers> list = playingcard.getAllPlayers();
		for(int i=0;i<6;i++){
			AllPlayers player = list.get(i);
				jbut[player.currentRow()][player.currentColumn()].setIcon(player.getImageIcon());
				jbut[player.currentRow()][player.currentColumn()].setVisible(true);
		}
		}
	
	public void setWeapons(){ //sets the position of all the weapons throughout the game board
		for(AllWeapons w: playingcard.getWeapons()){
			jbut[w.getRow()][w.getColumn()].setIcon(w.getImageIcon());
			jbut[w.getRow()][w.getColumn()].setVisible(true);
		}	
	}

	private void initializingCommandPanel(){//constructs the panel and its sub-panels used for all the controls and actions inthe game
		Image background = new ImageIcon(getClass().getResource("Backgroundpic.PNG")).getImage();
		
		commandpanel = new JPanel(){
			@Override
			public void paintComponent(Graphics g){
				super.paintComponent(g);
				g.drawImage(background,0,0,null);
			}
		};
		
		commandpanel.setLayout(new BoxLayout(commandpanel,BoxLayout.Y_AXIS));
		commandpanel.setPreferredSize(new Dimension(240,950));
		
		initializingTopCommandPanel();
		initializingMidCommandPanel();
		initializingBotCommandPanel();
		
		botmainpanel.add(commandpanel);
	}
	
	private void initializingTopCommandPanel(){//creates the top panel, in the command panel to show the players number of steps
		numberofsteps = new JPanel();
		numberofsteps.setPreferredSize(new Dimension(220,220));
		numberofsteps.setOpaque(false);
		
		JPanel topcommandpanel = new JPanel();
		topcommandpanel.add(numberofsteps);
		topcommandpanel.setOpaque(false);
		
		commandpanel.add(topcommandpanel);
	}
	
	private void initializingMidCommandPanel(){//constructs the middle panel, in the command panel to place and display the legal move controls
		movecommandpanel = new JPanel();
		movecommandpanel.setLayout(new GridLayout(2,3));
		movecommandpanel.setOpaque(false);;
			
		JPanel middlecommandpanel = new JPanel();	
		middlecommandpanel.add(movecommandpanel);
		middlecommandpanel.setOpaque(false);
		
		commandpanel.add(middlecommandpanel);
	}
	
	private void initializingBotCommandPanel(){//constructs the bottom panel, in the command panel to create the place for the actions that the player can make(ex. Make Accusation)
		actioncommandpanel = new JPanel();
		actioncommandpanel.setLayout(new GridLayout(7,0));
		actioncommandpanel.setOpaque(false);
		
		JPanel botcommandpanel = new JPanel();
		botcommandpanel.setOpaque(false);
		
		JPanel emptypanel = new JPanel();
		emptypanel.setPreferredSize(new Dimension(220,70));
		emptypanel.setVisible(true);
		emptypanel.setOpaque(false);
		
		botcommandpanel.add(emptypanel);
		botcommandpanel.add(actioncommandpanel);
		
		commandpanel.add(botcommandpanel);
	}
	
	public void addButtonsToTheMoveCommandPanel() {//adds the buttons for legal moves to the commandpanel
		movecommandpanel.removeAll();
		
		JButton moveup = new JButton(new ImageIcon(getClass().getResource("up.PNG")));
		moveup.setPreferredSize(new Dimension(70,70));
		moveup.setOpaque(false);
		moveup.setContentAreaFilled(false);
		moveup.setBorderPainted(false);
		
		JButton movedown = new JButton(new ImageIcon(getClass().getResource("Down.PNG")));
		movedown.setPreferredSize(new Dimension(75,70));
		movedown.setOpaque(false);
		movedown.setContentAreaFilled(false);
		movedown.setBorderPainted(false);
		
		
		JButton moveleft = new JButton(new ImageIcon(getClass().getResource("left.PNG")));
		moveleft.setPreferredSize(new Dimension(70,70));
		moveleft.setOpaque(false);
		moveleft.setContentAreaFilled(false);
		moveleft.setBorderPainted(false);
		
		JButton moveright = new JButton(new ImageIcon(getClass().getResource("right.PNG")));
		moveright.setPreferredSize(new Dimension(70,70));
		moveright.setOpaque(false);
		moveright.setContentAreaFilled(false);
		moveright.setBorderPainted(false);
		
		moveup.addActionListener(new MoveUpHandler(playingcard));
		movedown.addActionListener(new MoveDownHandler(playingcard));
		moveleft.addActionListener(new MoveLeftHandler(playingcard));
		moveright.addActionListener(new MoveRightHandler(playingcard));
		
		JButton emptybutton1 = new JButton();
		emptybutton1.setVisible(false);
		JButton emptybutton2 = new JButton();
		emptybutton2.setVisible(false);
		movecommandpanel.add(emptybutton1);
		movecommandpanel.add(moveup);
		movecommandpanel.add(emptybutton2);
		movecommandpanel.add(moveleft);
		movecommandpanel.add(movedown);
		movecommandpanel.add(moveright);
		window.repaint();
	}

	public void setCurrentPlayer() {//notifies and sets the current
		int index = playingcard.getIndexOfCurrentPlayer();
		al.get(index).setBorder(new MatteBorder(4,4,4,4,Color.BLACK));
		
	}
	
	public void setSteps(){//displays the steps the player can make in the result of the dice roll in the top command panel
		numberofsteps.removeAll();
		JLabel stepsbox = new JLabel(new ImageIcon(getClass().getResource("StepsBox.PNG")));
		stepsbox.setLayout(new BorderLayout());
		
		JLabel label = new JLabel("Number of Steps: "+ playingcard.getCurrentPlayer().getAvailableSpacesToTake());
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setFont(label.getFont().deriveFont(Font.BOLD,18));
			
		stepsbox.add(label);		
		numberofsteps.add(stepsbox);
		window.validate();
	}
	
	@Override
	public void update(){//updates the game board when a change is made to the board(refreshes the GUI)
		setPlayerBoard();
		addButtonsToBoardPanel();
		addButtonsToTheActionCommandPanel();
		addButtonsToTheMoveCommandPanel();
		setCurrentPlayer();
		setSteps();
	}
	
	public JFrame getframe() {
		return window;
	}
}
