
package edu.buffalo.cse116;

import static org.junit.Assert.*;

import java.util.ArrayList;
import org.junit.Test;
import Characters.*;
import Exceptions.CollisionException;
import Exceptions.IllegalMoveException;
import Exceptions.MoveThroughWallException;
import Exceptions.OppositeMoveException;
import Exceptions.OutOfStepsException;
import PlayersOfGame.AllPlayers;
import Rooms.*;
import Weapons.*;

public class GameBoardTest {


	@Test	
	public void verticallMove() throws OutOfStepsException, CollisionException, OppositeMoveException, MoveThroughWallException, IllegalMoveException{// this tests proper vertical movement

		PlayingCards playingcard = new PlayingCards();
		AllPlayers player = playingcard.getAllPlayers().get(0);
		player.setSteps(2);
		player.setRow(21);
		player.setColumn(5);
		assertTrue(playingcard.moveUpWithAllSteps(player));
	}
	
	@Test
	public void horizontalMove() throws OutOfStepsException, CollisionException, OppositeMoveException, MoveThroughWallException, IllegalMoveException{//this tests whether you can properly move horizontally
	
		PlayingCards playingcard = new PlayingCards();
		AllPlayers player = playingcard.getAllPlayers().get(0);
		player.diceRoll();
		player.setRow(4);
		player.setColumn(0);
		assertTrue(playingcard.moveRightWithAllSteps(player));

	}
	
	@Test 
	public void moveHorizontallyAndVertically() throws OutOfStepsException, CollisionException, OppositeMoveException, MoveThroughWallException, IllegalMoveException{// this tests whether you can move horizontally and vertically properly in the same move
		PlayingCards playingcard = new PlayingCards();
		AllPlayers player = playingcard.getCurrentPlayer();
		player.setSteps(2);
		assertTrue(playingcard.moveDown());
		assertTrue(playingcard.moveRight());
	}

	@Test
	public void enteringRoomThroughDoor() throws OutOfStepsException, CollisionException, OppositeMoveException, MoveThroughWallException, IllegalMoveException{//this tests whether the player can enter through a doorway and not a wall in the proper way

		PlayingCards playingcard = new PlayingCards();
		AllPlayers player = playingcard.getCurrentPlayer();
		player.setSteps(20);
		assertTrue(playingcard.moveDown());
		assertTrue(playingcard.moveDown());
		assertTrue(playingcard.moveDown());
		assertTrue(playingcard.moveDown());
		assertTrue(playingcard.moveDown());
		assertTrue(playingcard.moveRight());
		assertTrue(playingcard.moveRight());
		assertTrue(playingcard.moveRight());
		assertTrue("" + player.currentPosition() + " " + player.currentRow() + " " + player.currentColumn(),
				playingcard.moveUp());
	}

	@Test
	public void useSecretPassageway() { // this tests whether the player can
										// traverse the corner rooms of the
										// board properly
		PlayingCards playingcard = new PlayingCards();
		AllPlayers allps = playingcard.getAllPlayers().get(0);
		allps.diceRoll();
		allps.setRow(0);
		allps.setColumn(0);
		allps.setCurrentPosition("Study");
		assertTrue(playingcard.secretpassageway());
	}

	@Test
	public void moveMoreSquaresThanTheDieRoll() throws OutOfStepsException, CollisionException, OppositeMoveException, MoveThroughWallException, IllegalMoveException{ //this tests whether you have run out of steps and you can no longer move properly
		PlayingCards playingcard = new PlayingCards();
		AllPlayers player = playingcard.getCurrentPlayer();
		player.diceRoll();
		try {
			assertTrue(playingcard.moveDownWithAllSteps(player));
		} catch (OutOfStepsException e) {
			fail();
		}
		try {
			assertTrue("You Are Out of Steps", playingcard.moveDown());
		} catch (OutOfStepsException e) {
			// assertTrue(true);
		}
	}
	
	@Test
	public void moveDiagonally(){//tests whether a diagonal move was attempted and returns false
		PlayingCards pcs = new PlayingCards();
		assertFalse(pcs.moveDiagonally());
	}
	
	@Test
	public void contiguousMove(){
		//this tests whether a contiguous move is illegal
	}
	
	@Test
	public void moveThroughAWall() throws OutOfStepsException, CollisionException, OppositeMoveException, MoveThroughWallException, IllegalMoveException{//tests whether the player attempted to move into a room without using the doorspace and returns false if the player tried to go through a wall
		PlayingCards playingcard = new PlayingCards();
		AllPlayers player = playingcard.getAllPlayers().get(0);
		player.diceRoll();
		assertFalse(""+player.currentPosition(),playingcard.moveLeft());
	}

	@Test
	public void suggestionRotation() throws OutOfStepsException, CollisionException, OppositeMoveException, MoveThroughWallException, IllegalMoveException{
		PlayingCards playcards = new PlayingCards();
		ArrayList<AllCards> weapons = playcards.getWeaponCards();
		ArrayList<AllCards> characters = playcards.getCharacterCards();
		ArrayList<AllCards> rooms = playcards.getRoomCards();

		AllCharacters character = (AllCharacters) playcards.mystery.get(0);
		AllWeapons weapon = (AllWeapons) playcards.mystery.get(1);
		AllRooms room = (AllRooms) playcards.mystery.get(2);

		AllPlayers player = playcards.getCurrentPlayer();

		player.setSteps(20);
		assertTrue("" + player.currentPosition() + " " + player.currentRow() + " " + player.currentColumn(),
				playcards.moveDown());
		assertTrue("" + player.currentPosition() + " " + player.currentRow() + " " + player.currentColumn(),
				playcards.moveDown());
		assertTrue("" + player.currentPosition() + " " + player.currentRow() + " " + player.currentColumn(),
				playcards.moveDown());
		assertTrue("" + player.currentPosition() + " " + player.currentRow() + " " + player.currentColumn(),
				playcards.moveDown());
		assertTrue("" + player.currentPosition() + " " + player.currentRow() + " " + player.currentColumn(),
				playcards.moveDown());
		assertTrue("" + player.currentPosition() + " " + player.currentRow() + " " + player.currentColumn(),
				playcards.moveRight());
		assertTrue("" + player.currentPosition() + " " + player.currentRow() + " " + player.currentColumn(),
				playcards.moveRight());
		assertTrue("" + player.currentPosition() + " " + player.currentRow() + " " + player.currentColumn(),
				playcards.moveRight());
		assertTrue("" + player.currentPosition() + " " + player.currentRow() + " " + player.currentColumn(),
				playcards.moveUp());

		assertTrue(playcards.suggestionRotation((AllCharacters) characters.get(0), weapon, room));
		assertTrue(playcards.suggestionRotation((AllCharacters) characters.get(1), weapon, room));
		assertTrue(playcards.suggestionRotation((AllCharacters) characters.get(2), weapon, room));
		assertTrue(playcards.suggestionRotation((AllCharacters) characters.get(3), weapon, room));
		assertTrue(playcards.suggestionRotation((AllCharacters) characters.get(4), weapon, room));
		assertTrue(playcards.suggestionRotation(character, (AllWeapons) weapons.get(0), room));
		assertTrue(playcards.suggestionRotation(character, (AllWeapons) weapons.get(1), room));
		assertTrue(playcards.suggestionRotation(character, (AllWeapons) weapons.get(2), room));
		assertTrue(playcards.suggestionRotation(character, (AllWeapons) weapons.get(3), room));
		assertTrue(playcards.suggestionRotation(character, (AllWeapons) weapons.get(4), room));
		assertTrue(playcards.suggestionRotation(character, weapon, (AllRooms) rooms.get(0)));
		assertTrue(playcards.suggestionRotation(character, weapon, (AllRooms) rooms.get(1)));
		assertTrue(playcards.suggestionRotation(character, weapon, (AllRooms) rooms.get(2)));
		assertTrue(playcards.suggestionRotation(character, weapon, (AllRooms) rooms.get(3)));
		assertTrue(playcards.suggestionRotation(character, weapon, (AllRooms) rooms.get(4)));
		assertTrue(playcards.suggestionRotation(character, weapon, (AllRooms) rooms.get(5)));
		assertTrue(playcards.suggestionRotation(character, weapon, (AllRooms) rooms.get(6)));
		assertTrue(playcards.suggestionRotation(character, weapon, (AllRooms) rooms.get(7)));
		assertFalse(playcards.suggestionRotation(character, weapon, room));

		// assertTrue(playcards.individualSuggestionRotation(new MsPeacock(),
		// new LeadPipe(), new Study()));
		// assertTrue(playcards.individualSuggestionRotation(new MsPeacock(),
		// new Revolver(), new Study()));
		// assertTrue(playcards.individualSuggestionRotation(new MrsWhite(), new
		// LeadPipe(), new Library()));
		// assertFalse(playcards.individualSuggestionRotation(new MrGreen(), new
		// LeadPipe(), new Study()));
		// assertTrue(playcards.suggestionRotation(new MrGreen(), new
		// LeadPipe(), new Study()));
		// assertTrue(playcards.suggestionRotation(new MrsWhite(), new
		// LeadPipe(), new Library()));
		//
		// assertTrue("You were not able to gain any knowledge from the next
		// player.",playcards.suggestionRotation(new MrGreen(), new Rope(), new
		// Library()));
		//
		// assertFalse(playcards.suggestionRotation(new MrsWhite(), new
		// LeadPipe(), new Conservatory()));
		// assertFalse("You were given information, when you shouldn't have
		// found out anything.", playcards.suggestionRotation(new MrsWhite(),
		// new LeadPipe(), new Study()));
		//
		// assertTrue("You were not able to gain any knowledge from the next
		// player.",playcards.suggestionRotation(new MrGreen(), new Rope(), new
		// Library())); // This test takes care of whether the player would get
		// any information from the first player
		// assertTrue(playcards.individualSuggestionRotation(new MsPeacock(),
		// new LeadPipe(), new Study())); // This test takes care of if the
		// immediate player to the left of the player making the suggestion has
		// a player card.
		// assertTrue(playcards.individualSuggestionRotation(new MrsWhite(), new
		// Revolver(), new Study())); //This test takes care of whether the
		// immediate player to the left of the player making the suggestion has
		// a room card.
		// assertTrue(playcards.individualSuggestionRotation(new MrsWhite(), new
		// LeadPipe(), new Library())); // This test takes care of if the player
		// to the immediate left of the player making the suggestion has a room
		// card.
		// assertFalse(playcards.individualSuggestionRotation(new MrGreen(), new
		// LeadPipe(), new Study())); //This test takes care of whether the next
		// player has none of the cards suggested and that it should move to the
		// next player to ask them the same question.
		// assertTrue(playcards.suggestionRotation(new MrGreen(), new
		// Revolver(), new Library())); //This test takes care of the suggestion
		// asking the next player because they have two matching cards to the
		// starting players suggestion.
		// assertTrue(playcards.suggestionRotation(new MrsWhite(), new
		// LeadPipe(), new Library())); //This test takes care of the suggestion
		// if it reached the next player after the first next player if they
		// have one matching card.
		// assertFalse("You were given information, when you shouldn't have
		// found out anything.", playcards.suggestionRotation(new MrsWhite(),
		// new LeadPipe(), new Study())); // This test takes care of the fact
		// that the suggestion cannot be answered by anyone of the opposing
		// players.
	}

	public void collision() throws OutOfStepsException, CollisionException, OppositeMoveException, MoveThroughWallException, IllegalMoveException{//tests whether one players tries to occupy the same space in the hallway that another player already occupies
		PlayingCards playingcard = new PlayingCards();
		AllPlayers player = playingcard.getAllPlayers().get(0);
		AllPlayers player1 = playingcard.getAllPlayers().get(1);
		player.diceRoll();
		player1.setRow(2);
		player1.setColumn(6);
		assertTrue(playingcard.moveDown());
		assertFalse(playingcard.moveDown());
	}

}
