package ButtonHandlers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import edu.buffalo.cse116.GUI;
import edu.buffalo.cse116.PlayingCards;
//handles what happens if the player wants to use the secret passage as a result of button press
public class SecretPassageHandler implements ActionListener {
	private GUI _gameboard;
	private PlayingCards _players;

	public SecretPassageHandler(PlayingCards players, GUI gameboard) {
		_gameboard = gameboard;
		_players = players;
	}
	//updates the game board after the button is pressed to display the players new position in respect to the game board and appropriate rooms
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		_players.secretpassageway();
		_gameboard.update();
	}

}
