package ButtonHandlers;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import PlayersOfGame.AllPlayers;
import edu.buffalo.cse116.AllCards;
import edu.buffalo.cse116.PlayingCards;
//handles what happens if the current player wants to see what is in their hand after pressing the button in the GUI
public class ShowCardsHandler implements ActionListener {

	private PlayingCards pc;

	public ShowCardsHandler(PlayingCards ps) {
		pc = ps;
	}
//creates a window that will display the current players hand and all the cards that they have for the game after pressing the button
	@Override
	public void actionPerformed(ActionEvent arg0) {
		JFrame window = new JFrame("Your Hand");
		JPanel mainpanel = new JPanel();
		window.setSize(380, 260);
		mainpanel.setLayout(new BorderLayout());
		JPanel playername = new JPanel();
		JPanel cards = new JPanel();
		JLabel c1but = new JLabel();
		c1but.setSize(new Dimension(200, 125));
		JLabel c2but = new JLabel();
		JLabel c3but = new JLabel();

		mainpanel.add(playername, BorderLayout.NORTH);
		mainpanel.add(cards, BorderLayout.CENTER);

		AllPlayers currentplayer = pc.getCurrentPlayer();
		ArrayList<AllCards> hand = currentplayer.getCards();

		c1but.setToolTipText(hand.get(0).getName());
		c2but.setToolTipText(hand.get(1).getName());
		c3but.setToolTipText(hand.get(2).getName());

		try {
			ImageIcon img1 = new ImageIcon(getClass().getResource(hand.get(0).getImage()));
			c1but.setIcon(img1);
		} catch (NullPointerException b) {
			b.printStackTrace();
		}
		try {
			ImageIcon img1 = new ImageIcon(getClass().getResource(hand.get(1).getImage()));
			c2but.setIcon(img1);
		} catch (NullPointerException b) {
			b.printStackTrace();
		}
		try {
			ImageIcon img1 = new ImageIcon(getClass().getResource(hand.get(2).getImage()));
			c3but.setIcon(img1);
		} catch (NullPointerException b) {
			b.printStackTrace();
		}

		playername.add(new JLabel(pc.getCurrentPlayer().getcharacterName()));
		cards.setLayout(new GridLayout(0, 3, 3, 0));

		cards.add(c1but);
		cards.add(c2but);
		cards.add(c3but);

		window.setContentPane(mainpanel);

		window.validate();
		window.setVisible(true);
		window.setResizable(false);
		window.setLocationRelativeTo(null);

	}

}
