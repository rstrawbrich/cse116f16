package ButtonHandlers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import Exceptions.*;
import edu.buffalo.cse116.PlayingCards;
//handles what happens if the player wants to move left ( as result of button press) in the hallway
public class MoveLeftHandler implements ActionListener {

	private PlayingCards _playingcards;

	public MoveLeftHandler(PlayingCards playingcards){

		_playingcards = playingcards;
	}

	@Override
	public void actionPerformed(ActionEvent e) {//handles whether the result of moving left violates one of the principle moving rules of the game
		// TODO Auto-generated method stub
		try {
			_playingcards.moveLeft();
		} catch (OutOfStepsException e1) {
			JOptionPane.showMessageDialog(null, "You ran out of steps!!!");
		} catch (CollisionException e2) {
			JOptionPane.showMessageDialog(null, "You can't move into a player!!");
		} catch (OppositeMoveException e3) {
			JOptionPane.showMessageDialog(null, "You can't move in a opposite direction!!");
		} catch (MoveThroughWallException e4) {
			JOptionPane.showMessageDialog(null, "Please enter a room through a door!");
		} catch (IllegalMoveException e5){
			JOptionPane.showMessageDialog(null, "You can't move to MysterySpots!");
		} catch (ArrayIndexOutOfBoundsException e6){
			JOptionPane.showMessageDialog(null, "Move outside the gameboard is illegal!");
		}
	}

}
