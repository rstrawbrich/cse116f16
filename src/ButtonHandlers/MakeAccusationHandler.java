package ButtonHandlers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import Characters.AllCharacters;
import Characters.ColMustard;
import Characters.MrGreen;
import Characters.MrsWhite;
import Characters.MsPeacock;
import Characters.MsScarlet;
import Characters.ProfPlum;
import Exceptions.MustRollDiceFirstException;
import PlayersOfGame.AllPlayers;
import Rooms.AllRooms;
import Rooms.Ballroom;
import Rooms.BillardsRoom;
import Rooms.Conservatory;
import Rooms.DiningRoom;
import Rooms.Hall;
import Rooms.Kitchen;
import Rooms.Library;
import Rooms.Lounge;
import Rooms.Study;
import Weapons.AllWeapons;
import edu.buffalo.cse116.AllCards;
import edu.buffalo.cse116.GUI;
import edu.buffalo.cse116.PlayingCards;
//handles making the final accusation of the game
public class MakeAccusationHandler implements ActionListener {

	private GUI _gui;
	private PlayingCards _playingcards;
	private ArrayList<AllCharacters> characters;
	private ArrayList<AllWeapons> weapons;
	private ArrayList<AllRooms> rooms;
	private ArrayList<AllCards> _cards;
	private AllPlayers _players;
	private int index;
	private AllCharacters chars;
	private AllWeapons wps;
	private AllRooms rms;
	private JFrame window;
	public static final String[] _characters = { "MrGreen", "MsPeacock", "ProfPlum", "MsScarlet", "MrsWhite",
			"ColMustard" };
	public static final String[] _weapons = { "LeadPipe", "Revolver", "Wrench", "CandleStick", "Knife", "Rope" };
	public static final String[]_rooms = {"Hall", "Lounge", "BallRoom", "Study", "Kitchen", "Conservatory", "BillardsRoom", "DiningRoom", "Library"};


	public MakeAccusationHandler(PlayingCards playingcards, GUI gui) {
		_playingcards = playingcards;
		_gui = gui;

	}

	@Override
	public void actionPerformed(ActionEvent e) {//handles asking the player what their accusation is, what happens if the accusation is right, and what happens when the accusation is wrong

		_cards = new ArrayList<AllCards>();
		_players = new AllPlayers(_cards, index);
		AllCharacters[] character = new AllCharacters[] { new MsScarlet(), new MrGreen(), new ColMustard(),
				new MsPeacock(), new ProfPlum(), new MrsWhite() };
		AllRooms[] room = new AllRooms[] { new Conservatory(), new Lounge(), new Hall(), new Study(), new Kitchen(),
				new BillardsRoom(), new DiningRoom(), new Ballroom(), new Library() };
		characters = new ArrayList<AllCharacters>();
		rooms = new ArrayList<AllRooms>();
		weapons = _playingcards.getWeapons();
		chars = new AllCharacters();
		wps = new AllWeapons();
		characters.addAll(Arrays.asList(character));
		rooms.addAll(Arrays.asList(room));

		String _character = (String) JOptionPane.showInputDialog(window, "Who do you think it is?",
				"Character suggestion", JOptionPane.QUESTION_MESSAGE, null, _characters, _characters[0]);
		String _weapon = (String) JOptionPane.showInputDialog(window, "What weapon do you think it is?",
				"Weapon suggestion", JOptionPane.QUESTION_MESSAGE, null, _weapons, _weapons[0]);
		String _room = (String) JOptionPane.showInputDialog(window, "Where do you think it happened?",
				"Room suggestion", JOptionPane.QUESTION_MESSAGE, null, _rooms, _rooms[0]);

		for (int i = 0; i < characters.size(); i += 1) {
			if (characters.get(i).getName().equals(_character)) {
				chars = characters.get(i);
			}
		}
		for (int j = 0; j < weapons.size(); j += 1) {
			if (weapons.get(j).getName().equals(_weapon)) {
				wps = weapons.get(j);
			}
		}
		for (int z = 0; z < rooms.size(); z += 1) {
			if (rooms.get(z).getName().equals(_room)) {
				rms = rooms.get(z);
			}
		}
		if(_playingcards.makeAccusation(chars, wps, rms)){
			JOptionPane.showMessageDialog(null, "Congratulations, you have won the game." + "It was "+_character+"with the "+_weapon+"in the "+_room+".");
			int confirm = JOptionPane.showConfirmDialog(null, "Do you want to play again?");
			if(confirm == JOptionPane.YES_OPTION){
				_gui.getframe().dispose();
				new GUI(_playingcards);
			}
			else {_gui.getframe().dispose();}
		}
		else{
			JOptionPane.showMessageDialog(null, "I'm very sorry, but you accusation is false and you have lost the game. It was not "+_character+" with the"+_weapon+" in the "+_room+"." + "Thank you for playing.");
			_playingcards.getCurrentPlayer().setPlayerState(false);
			_playingcards.getCurrentPlayer().diceRoll();
			try {
				_playingcards.endPlayersTurn();
			} catch (MustRollDiceFirstException e1) {
				
			}
			
			
		}
	}
}
