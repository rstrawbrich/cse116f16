package ButtonHandlers;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import Characters.*;
import Rooms.*;
import Weapons.AllWeapons;
import edu.buffalo.cse116.AllCards;
import edu.buffalo.cse116.GUI;
import edu.buffalo.cse116.PlayingCards;
//handles what will result if the player presses the suggestion button
public class SuggestionHandler implements ActionListener {
	private GUI _gui;
	private PlayingCards _playingcards;
	public static final String[] _characters = { "MrGreen", "MsPeacock", "ProfPlum", "MsScarlet", "MrsWhite",
			"ColMustard" };
	public static final String[] _weapons = { "LeadPipe", "Revolver", "Wrench", "CandleStick", "Knife", "Rope" };


	public SuggestionHandler(PlayingCards playingcards, GUI gui) {
		_playingcards = playingcards;
		_gui = gui;

	}
//when the button is pressed, a set of windows will pop-up allowing the player to enter in their suggestion through the set of drop down menus to gain information about whether their suggestion is true or false in respect to making a final accusation
	@Override
	public void actionPerformed(ActionEvent e) {
		String curpos = _playingcards.getCurrentPlayer().currentPosition();
		if(_playingcards.alreadyMakeSuggestion()){
				JOptionPane.showMessageDialog(null, "You already made a suggestion!");
		} else if(curpos.equals("Hallway")||curpos.equals("doorspace")){
			JOptionPane.showMessageDialog(null, "Please enter a room in order to make suggestion.");
		} else {
			
			AllCharacters[] character = new AllCharacters[]{new MsScarlet(), new MrGreen(), new ColMustard(), new MsPeacock(), new ProfPlum(), new MrsWhite()};
			AllRooms[] room = new AllRooms[]{new Conservatory(), new Lounge(), new Hall(), new Study(), new Kitchen(), new BillardsRoom(), new DiningRoom(), new Ballroom(), new Library()};			
			ArrayList<AllCharacters> characters = new ArrayList<AllCharacters>();
			ArrayList<AllRooms> rooms = new ArrayList<AllRooms>();
			ArrayList<AllWeapons> weapons = _playingcards.getWeapons();
			
			
			characters.addAll(Arrays.asList(character));
			rooms.addAll(Arrays.asList(room));
			String[] _rooms = new String[] {_playingcards.getCurrentPlayer().currentPosition()};
			
			String _character = (String) JOptionPane.showInputDialog(null, "Who do you think it is?", "Character suggestion", JOptionPane.QUESTION_MESSAGE,null,_characters,_characters[0]);
			String _weapon = (String) JOptionPane.showInputDialog(null, "What weapon do you think it is?", "Weapon suggestion", JOptionPane.QUESTION_MESSAGE,null, _weapons, _weapons[0]);
			String _room = (String) JOptionPane.showInputDialog(null, "Where do you think it happened?", "Room suggestion", JOptionPane.QUESTION_MESSAGE,null,_rooms, _rooms[0]);
	
			AllCharacters chars = new AllCharacters();
			AllWeapons wps = new AllWeapons();
			AllRooms rms = new AllRooms();
			if(_character!=null&&_weapon!=null&&_room!=null){		
				for(int i=0; i<characters.size(); i+=1){
					if(characters.get(i).getName().equals(_character)){
						chars = characters.get(i);
						}
					}
			
				for(int j=0; j<weapons.size(); j+=1){
					if(weapons.get(j).getName().equals(_weapon)){
						wps = weapons.get(j);
					}
				}
				for(int z = 0; z<rooms.size(); z+=1){
					if(rooms.get(z).getName().equals(_room)){
						rms = rooms.get(z);
					}
				}
			
				_playingcards.suggestionRotation(chars, wps, rms);
				String player = _playingcards.getPlayerhasthecard();
				ArrayList<AllCards> exposedcard = _playingcards.getExposedCards();
				
				
				JFrame window = new JFrame();
				JPanel panel = new JPanel();
				
				
				window.setLayout(new BorderLayout());
				
				window.add(panel,BorderLayout.NORTH);
				
				JPanel playerpanel = new JPanel();
				JPanel cardpanel = new JPanel();
						
						
						
				window.add(playerpanel,BorderLayout.NORTH);
				window.add(cardpanel,BorderLayout.CENTER);
				
				window.setVisible(true);
				window.setSize(400, 300);
				
				playerpanel.add(new JLabel(player));
						
				if(exposedcard.size()==1){
					JButton choosecard1 = new JButton();
					cardpanel.add(choosecard1);
					choosecard1.setText("?");
					choosecard1.addActionListener(new InnerSuggestionHandler(choosecard1,exposedcard.get(0).getImage(),window,cardpanel));
				}
				if(exposedcard.size()==2){
					JButton choosecard1 = new JButton();
					cardpanel.add(choosecard1);
					choosecard1.setText("?");
					choosecard1.addActionListener(new InnerSuggestionHandler(choosecard1,exposedcard.get(0).getImage(),window,cardpanel));
							
					JButton choosecard2 = new JButton();
					cardpanel.add(choosecard2);
					choosecard2.setText("?");
					choosecard2.addActionListener(new InnerSuggestionHandler(choosecard2, exposedcard.get(1).getImage(),window,cardpanel));
				}
				if(exposedcard.size()==3){
					JButton choosecard1 = new JButton();
					cardpanel.add(choosecard1);
					choosecard1.setText("?");
					choosecard1.addActionListener(new InnerSuggestionHandler(choosecard1,exposedcard.get(0).getImage(),window,cardpanel));
								
					JButton choosecard2 = new JButton();
					cardpanel.add(choosecard2);
					choosecard2.setText("?");
					choosecard2.addActionListener(new InnerSuggestionHandler(choosecard2, exposedcard.get(1).getImage(),window,cardpanel));
								
					JButton choosecard3 = new JButton();
					cardpanel.add(choosecard3);
					choosecard3.setText("?");
					choosecard3.addActionListener(new InnerSuggestionHandler(choosecard3, exposedcard.get(2).getImage(),window,cardpanel));
				}
						
					_gui.update();
//					window.repaint();
					window.validate();
				} else {
					JOptionPane.showMessageDialog(null, "You have canceled your suggestion.");
				}
			}				
	}
}
