package ButtonHandlers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import Exceptions.MustRollDiceFirstException;
import edu.buffalo.cse116.PlayingCards;
//handles ending the player's turn
public class EndTurnHandler implements ActionListener {

	private PlayingCards _playingcards;

	public EndTurnHandler(PlayingCards playingcards){
		_playingcards= playingcards;

	}
//makes it so that the player rolls the dice before ending their turn
	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			_playingcards.endPlayersTurn();
			JOptionPane.showMessageDialog(null, _playingcards.getCurrentPlayer().getcharacterName()+"'s turn.");
		} catch (MustRollDiceFirstException mrde){
			JOptionPane.showMessageDialog(null, "You need to roll the dice first.");
		}
	}

	
}
