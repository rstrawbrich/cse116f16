package ButtonHandlers;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

//handles the result of the suggestion handler to produce outputs that the player can choose from when inputs are given to the suggestion handler
public class InnerSuggestionHandler implements ActionListener {
	private JButton button;
	private String text;
	private JFrame _window;
	private JPanel panel;

	public InnerSuggestionHandler(JButton jb, String s, JFrame j, JPanel p) {
		text = s;
		button = jb;
		_window = j;
		panel = p;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		panel.removeAll();
		button.removeAll();
		panel.add(button);
		ImageIcon img = new ImageIcon(getClass().getResource(text));
		button.setPreferredSize(new Dimension(img.getIconWidth() - 5, img.getIconHeight()));
		button.setIcon(img);
		_window.repaint();

	}

}
