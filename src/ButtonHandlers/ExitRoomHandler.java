package ButtonHandlers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import Exceptions.OutOfStepsException;
import edu.buffalo.cse116.PlayingCards;

//handles the player when they exit the room
public class ExitRoomHandler implements ActionListener{

	private PlayingCards players;

	public ExitRoomHandler(PlayingCards player){

		players = player;
	}
//makes it so that the player cannot exit a room until their next turn if they have just entered that room in which they are trying to exit
	@Override
	public void actionPerformed(ActionEvent arg0) {
		try{
			players.exitRoom();
		} catch (OutOfStepsException o1){
			JOptionPane.showMessageDialog(null, "You have entered the room, you cannot leave until your next turn");
		}
	}

}
