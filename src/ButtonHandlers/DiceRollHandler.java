package ButtonHandlers;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import Dice.Dice;
import edu.buffalo.cse116.GUI;
import edu.buffalo.cse116.PlayingCards;
//This handles whether the person rolls the dice and says whether the person has already rolled the dice this turn or not.
public class DiceRollHandler implements ActionListener {
	private PlayingCards _ps;
	private GUI _gui;
	
	public DiceRollHandler(PlayingCards ps,GUI gui){
		_ps = ps;
		_gui = gui;
	}

	@Override
	public void actionPerformed(ActionEvent arg0){

		if(_ps.getCurrentPlayer().diceRoll()){
			int steps = _ps.getCurrentPlayer().getAvailableSpacesToTake();
		
			new Dice(steps, _gui);
		} else {
			JOptionPane.showMessageDialog(null, "You already rolled the dice, please wait until next turn to roll again.");
		}
	}

}
