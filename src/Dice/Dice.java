package Dice;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

import edu.buffalo.cse116.GUI;

public class Dice {
	
	public static int ROLLING_TIME = 10;
	private int time,_steps;
	private JFrame window;
	private JPanel mainpanel;
	private JLabel but1;
	private Timer timer,timer2;
	private GUI gui;
	
	private ArrayList<ImageIcon> images;
	 public Dice(int steps,GUI _gui){
		 window = new JFrame();
		 mainpanel = new JPanel();
		 time = 0;
		 _steps = steps;
		 gui = _gui;
		 nextImg();
		 
		 window.setLocationRelativeTo(null);
		 window.setContentPane(mainpanel);
		 window.setVisible(true);
		 window.pack();
	 }
	 
	 public void nextImg(){
		 images = new ArrayList<ImageIcon>();
		 images.add(new ImageIcon(getClass().getResource("1.PNG")));
		 images.add(new ImageIcon(getClass().getResource("2.PNG")));
		 images.add(new ImageIcon(getClass().getResource("3.PNG")));
		 images.add(new ImageIcon(getClass().getResource("4.PNG")));
		 images.add(new ImageIcon(getClass().getResource("5.PNG")));
		 images.add(new ImageIcon(getClass().getResource("6.PNG")));
		 
		 mainpanel.setLayout(new BorderLayout());
		 but1 = new JLabel(images.get(0));

		 mainpanel.setPreferredSize(new Dimension(250,250));
		 mainpanel.add(but1,BorderLayout.CENTER);
		 
		 timer = new Timer(100, new timerHandler());
		 timer2 = new Timer(1000,new timerHandler2());
		 timer.start();
	 }
	 
	public class timerHandler implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent arg0) {
			Random random = new Random();
			int i = random.nextInt(6);
			but1.setIcon(images.get(i));
			time += 1;
			if(time == ROLLING_TIME){
				timer.stop();

				timer2.start();
				but1.setIcon(images.get(_steps-1));
				JOptionPane.showMessageDialog(null, "You roll the dice and you get "+_steps);
				gui.setSteps();
				
			}
		}
		
	}
	public class timerHandler2 implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent arg0) {
			timer2.stop();
			window.dispatchEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSING));
		}
	}	 
}

	