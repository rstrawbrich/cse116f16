package Characters;

public class ColMustard extends AllCharacters {

	public ColMustard() {
		super();
	}

	@Override
	public String getName() {
		return "ColMustard";
	}

	@Override
	public String getImage() {
		return "ColMustard.PNG";
	}

}
