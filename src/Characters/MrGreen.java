package Characters;

public class MrGreen extends AllCharacters {

	public MrGreen() {
		super();
	}

	@Override
	public String getName() {
		return "MrGreen";
	}

	@Override
	public String getImage() {
		return "MrGreen.PNG";
	}
}
