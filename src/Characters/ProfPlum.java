package Characters;

public class ProfPlum extends AllCharacters {

	public ProfPlum() {
		super();
	}

	@Override
	public String getName() {
		return "ProfPlum";
	}

	@Override
	public String getImage() {
		return "ProfPlum.PNG";
	}

}
