package Characters;

public class MsPeacock extends AllCharacters {

	public MsPeacock() {
		super();
	}

	@Override
	public String getName() {
		return "MsPeacock";
	}

	@Override
	public String getImage() {
		return "MsPeacock.PNG";
	}
}
