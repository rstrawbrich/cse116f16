package Characters;

public class MrsWhite extends AllCharacters {

	public MrsWhite() {
		super();
	}

	@Override
	public String getName() {
		return "MrsWhite";
	}

	@Override
	public String getImage() {
		return "MrsWhite.PNG";
	}
}
