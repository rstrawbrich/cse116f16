package Characters;

public class MsScarlet extends AllCharacters {

	public MsScarlet() {
		super();
	}

	@Override
	public String getName() {
		return "MsScarlet";
	}

	@Override
	public String getImage() {
		return "MsScarlet.PNG";
	}

}
