package Characters;

import Weapons.*;

import Rooms.*;

import edu.buffalo.cse116.AllCards;

public class AllCharacters extends AllCards {

	public AllCharacters() {
		super();
	}

	public boolean suggestionSentence(AllCharacters c, AllWeapons w, AllRooms r) {

		return false;

	}

	public void threeCards() {
		// put one character card, one weapon card, one room card into the
		// envelope
	}

	public void cardDistribution() {
		// distribute the remaining cards to each players
	}

	public AllCharacters getCharacters() {
		return new MrGreen(); // return the current player's character cards
	}

	public AllRooms getRooms() {
		return new Study(); // return the current player's room cards
	}

	public AllWeapons getWeapons() {
		return new Wrench(); // returns the current player's weapon cards
	}

	@Override
	public String getImage() {
		return "";
	}

}
