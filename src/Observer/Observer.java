package Observer;

public interface Observer {//observer setup so that the GUI can update as the player makes their moves

	public void update();
}
