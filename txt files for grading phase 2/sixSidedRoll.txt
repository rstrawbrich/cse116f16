sixSidedRoll.txt

    Explain how to see you have code which correctly generate a die/dice roll;
    
    * In the edu.buffalo.cse116 package, there is a DiceRoll class that defines how the dice roll number should be generated.
    * The number for the dice is defined to only show numbers 1 through 6, inclusive.
    
    Explain where the die/dice roll for Miss Scarlet (the first player) is displayed when the program begins;
    
    * When the Driver is run and the GUI is displayed, MsScarlet should be indicated as the first player in the top left of the 
    screen and the number of moves should be at first zero.
    * When the Dice Roll button is pressed, a pop-up box should appear saying how
    many moves the current player has now. 
    * Underneath the player on the left side of the screen should also indicate the number of steps the player has/ has left.
    
     
    Explain where the updated die/dice roll for another player is displayed when their turn begins program begins.
    
    * When the End Turn Button is pressed after the die is rolled for the player before, the next player can press the Dice Roll button and receive the same
    pop-up box and the updated number of steps to the left under the current player to the left of the GUI.
    * There should be a pop-up box when the DiceRoll Button is pressed and underneath the current player to the left should say the number 
    of steps the current player has at that moment.
   * This should continue through the rest of the players, when the die is rolled, the turn is ended, and the die for the next player is rolled again.
   * The numbers of steps should return back to zero at the start of each turn.