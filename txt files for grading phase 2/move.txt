move.txt

	Explain how to enter a move and see that a legal movement that stays within the hallway updates the player's location;
	
	*On the game board, one can enter a move by pressing either Move Up, Move Down, Move Left, or Move Right on the panel. If it is
	a legal move, the player's location will be updated on the game board to the space the player specified they wanted to move in.
	*If the player were to try to pass through the borders/ walls of the rooms the player will not move because they are only allow to enter 
	through the "doorspace" which is the gap in the border of the rooms.

	Explain how to enter a move and see that a legal movement that goes through a doorway and ends in a room updates the player's location.
	
	*Once the player is at a doorway, which are the spaces in front of the gaps in the borders of the rooms, they must use a step to enter the door, and then the character 
	will be placed randomly into the room, thus updating the player's location.

	Explain how to enter a move which uses a secret passageway updates the player's location;
	
	*Once the player is actually in a room, they can use the Take Secret Passage button to take the secret passage. This will place the 
	player's position at the room where the secret passage would take them (room on the complete opposite corner of the board).
	*The secret passage can only be used when the player has rolled the dice first and then clicked the button to move between the corner rooms 
	that are diagonally across from each other.

	Explain how illegal moves are prevented (including showing an error message if your program allows a user to try this).
	
	*If the player tries to make an illegal move in the hallway, essentially nothing will happen until the player makes a legal move. The console should display errors from thrown exceptions dealing with movement in the hallway.
	*If the player tried to end their turn without rolling the dice first a pop-up box will appear saying to roll the die first.
	*If you have entered a room, and then want to leave the room in the same turn, a pop - up box will appear that says you cannot leave the room until your next turn.
	*If you try to take use the secret passage way in a room that isn't a corner room, then nothing will happen because the players current location is not one of the four corner rooms
	appropriately travel through the secret passage way to